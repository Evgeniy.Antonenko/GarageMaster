﻿using System;

namespace GarageMaster.DAL.Entities.Contracts
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
