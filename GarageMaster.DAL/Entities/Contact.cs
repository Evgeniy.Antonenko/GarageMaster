﻿using System;
using GarageMaster.DAL.Entities.Contracts;

namespace GarageMaster.DAL.Entities
{
    public class Contact : IEntity
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FilePath { get; set; }
    }
}
