﻿using System;
using GarageMaster.DAL.Entities.Contracts;

namespace GarageMaster.DAL.Entities
{
    public class Rule : IEntity
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
