﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GarageMaster.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.DAL.Entities
{
    public class Role : IdentityRole<Guid>, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override Guid Id { get; set; }
    }
}
