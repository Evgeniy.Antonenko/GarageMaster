﻿using System;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.EntitiesConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GarageMaster.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, Guid>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<AboutProject> AboutProjects { get; set; }
        public DbSet<Rule> Rules { get; set; }

        public ApplicationDbContext(DbContextOptions options, IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(_entityConfigurationsContainer.ContactConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.AboutProjectConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.RuleConfiguration.ProvideConfigurationAction());
        }
    }
}
