﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GarageMaster.DAL.Migrations
{
    public partial class AddAboutProjectEntityAndAboutProjectConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AboutProjects",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(maxLength: 250, nullable: false),
                    Content = table.Column<string>(maxLength: 2147483647, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AboutProjects", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AboutProjects_Id",
                table: "AboutProjects",
                column: "Id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AboutProjects");
        }
    }
}
