﻿using GarageMaster.DAL.Entities;

namespace GarageMaster.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Contact> ContactConfiguration { get; }
        IEntityConfiguration<AboutProject> AboutProjectConfiguration { get; }
        IEntityConfiguration<Rule> RuleConfiguration { get; }
    }
}
