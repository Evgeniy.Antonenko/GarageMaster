﻿using GarageMaster.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration
{
    public class AboutProjectConfiguration : BaseEntityConfiguration<AboutProject>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<AboutProject> builder)
        {
            builder
                .Property(ap => ap.Title)
                .HasMaxLength(250)
                .IsRequired();

            builder
                .Property(ap => ap.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .HasIndex(ap => ap.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<AboutProject> builder)
        {
        }
    }
}
