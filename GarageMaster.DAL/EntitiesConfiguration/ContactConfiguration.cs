﻿using GarageMaster.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration
{
    public class ContactConfiguration : BaseEntityConfiguration<Contact>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Contact> builder)
        {
            builder
                .Property(au => au.Title)
                .HasMaxLength(250)
                .IsRequired();

            builder
                .Property(au => au.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .Property(au => au.Address)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .Property(au => au.Email)
                .HasMaxLength(50)
                .IsRequired();

            builder
                .Property(au => au.PhoneNumber)
                .HasMaxLength(13)
                .IsRequired();

            builder
                .Property(au => au.FilePath)
                .HasMaxLength(200)
                .IsRequired();

            builder
                .HasIndex(au => au.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Contact> builder)
        {
        }
    }
}
