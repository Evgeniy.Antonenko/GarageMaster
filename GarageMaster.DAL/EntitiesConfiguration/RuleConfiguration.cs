﻿using GarageMaster.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration
{
    public class RuleConfiguration : BaseEntityConfiguration<Rule>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Rule> builder)
        {
            builder
                .Property(r => r.Title)
                .HasMaxLength(250)
                .IsRequired();

            builder
                .Property(r => r.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .HasIndex(r => r.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Rule> builder)
        {
        }
    }
}
