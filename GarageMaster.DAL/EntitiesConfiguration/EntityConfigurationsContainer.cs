﻿using GarageMaster.DAL.Entities;
using GarageMaster.DAL.EntitiesConfiguration.Contracts;

namespace GarageMaster.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Contact> ContactConfiguration { get; }
        public IEntityConfiguration<AboutProject> AboutProjectConfiguration { get; }
        public IEntityConfiguration<Rule> RuleConfiguration { get; }


        public EntityConfigurationsContainer()
        {
            ContactConfiguration = new ContactConfiguration();
            AboutProjectConfiguration = new AboutProjectConfiguration();
            RuleConfiguration = new RuleConfiguration();
        }
    }
}
