﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GarageMaster.DAL.Entities.Contracts;
using GarageMaster.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace GarageMaster.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        private ApplicationDbContext _context;
        protected DbSet<T> entities;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
            entities = _context.Set<T>();

        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await entities.FindAsync(id);
        }

        public async Task CreateAsync(T entity)
        {
            await entities.AddAsync(entity);
        }

        public T Create(T entity)
        {
            var entityEntry = entities.Add(entity);
            _context.SaveChanges();
            return entityEntry.Entity;
        }

        public T GetById(Guid id)
        {
            return entities.FirstOrDefault(e => e.Id == id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return entities;
        }

        public T Update(T entity)
        {
            var entityEntry = _context.Update(entity);
            _context.SaveChanges();
            return entityEntry.Entity;
        }

        public void Remove(T entity)
        {
            entities.Remove(entity);
            _context.SaveChanges();
        }
    }
}
