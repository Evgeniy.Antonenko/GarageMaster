﻿using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Repositories.Contracts;

namespace GarageMaster.DAL.Repositories
{
    public class RuleRepository : Repository<Rule>, IRuleRepository
    {
        public RuleRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Rules;
        }
    }
}
