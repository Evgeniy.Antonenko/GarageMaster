﻿using GarageMaster.DAL.Entities;

namespace GarageMaster.DAL.Repositories.Contracts
{
    public interface IAboutProjectRepository : IRepository<AboutProject>
    {
    }
}
