﻿using GarageMaster.DAL.Entities;

namespace GarageMaster.DAL.Repositories.Contracts
{
    public interface IContactRepository : IRepository<Contact>
    {
    }
}
