﻿using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Repositories.Contracts;

namespace GarageMaster.DAL.Repositories
{
    public class AboutProjectRepository : Repository<AboutProject>, IAboutProjectRepository
    {
        public AboutProjectRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.AboutProjects;
        }
    }
}
