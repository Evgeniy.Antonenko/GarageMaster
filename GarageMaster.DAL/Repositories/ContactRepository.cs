﻿using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Repositories.Contracts;

namespace GarageMaster.DAL.Repositories
{
    public class ContactRepository : Repository<Contact>, IContactRepository
    {
        public ContactRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Contacts;
        }
    }
}
