﻿using System;
using System.Threading.Tasks;
using GarageMaster.DAL.Repositories;
using GarageMaster.DAL.Repositories.Contracts;

namespace GarageMaster.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IContactRepository Contacts { get; set; }
        public IAboutProjectRepository AboutProjects { get; set; }
        public IRuleRepository Rules { get; set; }
        //public IPostInTopicRepository PostInTopics { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Contacts = new ContactRepository(context);
            AboutProjects = new AboutProjectRepository(context);
            Rules = new RuleRepository(context);
            //PostInTopics = new PostInTopicRepository(context);
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
