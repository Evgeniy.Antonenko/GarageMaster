﻿
namespace GarageMaster.DAL
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
