﻿using System;
using GarageMaster.Models.AboutProjectModels;
using Microsoft.AspNetCore.Mvc;
using GarageMaster.Services.AboutProjects.Contracts;

namespace GarageMaster.Controllers
{
    public class AboutProjectController : Controller
    {
        private readonly IAboutProjectsService _aboutProjectsService;

        public AboutProjectController(IAboutProjectsService aboutProjectsService)
        {
            if (aboutProjectsService == null)
                throw new ArgumentNullException(nameof(aboutProjectsService));

            _aboutProjectsService = aboutProjectsService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var aboutProjectModel = _aboutProjectsService.GetAboutProject();

            return View(aboutProjectModel);
        }

        [HttpGet]
        public IActionResult CreateAboutProject()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateAboutProject(AboutProjectCreateModel model)
        {
            if (ModelState.IsValid)
            {
                _aboutProjectsService.CreateAboutProject(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult EditAboutProject()
        {
            var model = _aboutProjectsService.GetAboutProjectForEdit();

            return View(model);
        }

        [HttpPost]
        public IActionResult EditAboutProject(AboutProjectEditModel model)
        {
            if (ModelState.IsValid)
            {
                _aboutProjectsService.EditAboutProject(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }

        }
    }
}
