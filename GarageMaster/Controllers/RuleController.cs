﻿using System;
using GarageMaster.Models.RulesModels;
using Microsoft.AspNetCore.Mvc;
using GarageMaster.Services.Rules.Contracts;

namespace GarageMaster.Controllers
{
    public class RuleController : Controller
    {
        private readonly IRulesService _rulesService;

        public RuleController(IRulesService rulesService)
        {
            if (rulesService == null)
                throw new ArgumentNullException(nameof(rulesService));

            _rulesService = rulesService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var ruleModel = _rulesService.GetRule();

            return View(ruleModel);
        }

        [HttpGet]
        public IActionResult CreateRule()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateRule(RuleCreateModel model)
        {
            if (ModelState.IsValid)
            {
                _rulesService.CreateRule(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult EditRule()
        {
            var model = _rulesService.GetRuleForEdit();

            return View(model);
        }

        [HttpPost]
        public IActionResult EditRule(RuleEditModel model)
        {
            if (ModelState.IsValid)
            {
                _rulesService.EditRule(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }

        }
    }
}
