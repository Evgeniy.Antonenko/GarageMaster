﻿using Microsoft.AspNetCore.Mvc;
using System;
using GarageMaster.Models.ContactsModels;
using GarageMaster.Services.Contacts.Contracts;

namespace GarageMaster.Controllers
{
    public class ContactsController : Controller
    {
        private readonly IContactsService _contactsService;

        public ContactsController(IContactsService contactsService)
        {
            if (contactsService == null)
                throw new ArgumentNullException(nameof(contactsService));

            _contactsService = contactsService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var contactModel = _contactsService.GetContact();

            return View(contactModel);
        }

        [HttpGet]
        public IActionResult CreateContact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateContact(ContactCreateModel model)
        {
            if (ModelState.IsValid)
            {
                _contactsService.CreateContact(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult EditContact()
        {
            var model = _contactsService.GetContactForEdit();

            return View(model);
        }

        [HttpPost]
        public IActionResult EditContact(ContactEditModel model)
        {
            if (ModelState.IsValid)
            {
                _contactsService.EditContact(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }

        }
    }
}
