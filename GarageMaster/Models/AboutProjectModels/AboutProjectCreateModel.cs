﻿using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.AboutProjectModels
{
    public class AboutProjectCreateModel
    {
        [Required(ErrorMessage = "Поле \"Заголовок\" должно быть заполненно")]
        [Display(Name = "Заголовок:")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Поле \"Контент\" должно быть заполненно")]
        [Display(Name = "Контент:")]
        public string Content { get; set; }
    }
}
