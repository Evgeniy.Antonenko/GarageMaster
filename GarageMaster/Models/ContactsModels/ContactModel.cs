﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.ContactsModels
{
    public class ContactModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Заголовок:")]
        public string Title { get; set; }

        [Display(Name = "Контент:")]
        public string Content { get; set; }

        [Display(Name = "Адрес:")]
        public string Address { get; set; }

        [Display(Name = "e-mail:")]
        public string Email { get; set; }

        [Display(Name = "Номер телефона:")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Схема расположения")]
        public string FilePath { get; set; }
    }
}
