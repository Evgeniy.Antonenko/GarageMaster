﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace GarageMaster.Models.ContactsModels
{
    public class ContactEditModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Поле \"Заголовок\" должно быть заполненно")]
        [Display(Name = "Заголовок:")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Поле \"Контент\" должно быть заполненно")]
        [Display(Name = "Контент:")]
        public string Content { get; set; }

        [Required(ErrorMessage = "Поле \"Адрес\" должно быть заполненно")]
        [Display(Name = "Адрес:")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Поле \"e-mail\" должно быть заполненно")]
        [Display(Name = "e-mail:")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле \"Номер телефона\" должно быть заполненно")]
        [Display(Name = "Номер телефона")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Схема расположения")]
        public IFormFile UploadFile { get; set; }
    }
}
