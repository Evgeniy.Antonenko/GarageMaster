﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.RulesModels
{
    public class RuleEditModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Поле \"Заголовок\" должно быть заполненно")]
        [Display(Name = "Заголовок:")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Поле \"Контент\" должно быть заполненно")]
        [Display(Name = "Контент:")]
        public string Content { get; set; }
    }
}
