﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.RulesModels
{
    public class RuleModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Заголовок:")]
        public string Title { get; set; }

        [Display(Name = "Контент:")]
        public string Content { get; set; }
    }
}
