﻿using AutoMapper;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.AboutProjectModels;
using GarageMaster.Models.ContactsModels;
using GarageMaster.Models.RulesModels;

namespace GarageMaster
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateModelMap<Contact, ContactModel>();
            CreateModelMap<Contact, ContactCreateModel>();
            CreateModelMap<Contact, ContactEditModel>();
            CreateModelMap<AboutProject, AboutProjectModel>();
            CreateModelMap<AboutProject, AboutProjectCreateModel>();
            CreateModelMap<AboutProject, AboutProjectEditModel>();
            CreateModelMap<Rule, RuleModel>();
            CreateModelMap<Rule, RuleCreateModel>();
            CreateModelMap<Rule, RuleEditModel>();
            //CreateDishToDishModelMap();
            //CreateModelMap<Restaurant, RestaurantModel>();
            //CreateModelMap<Restaurant, RestaurantCreateModel>();
            //CreateModelMap<Dish, DishCreateModel>();
            //CreateModelMap<OrderBasket, OrderBasketModel>();
            //CreateModelMap<DishModel, OrderBasketDetailsModel>();
        }

        private void CreateModelMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }
    }
}
