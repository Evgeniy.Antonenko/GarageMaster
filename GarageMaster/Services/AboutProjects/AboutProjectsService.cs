﻿using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.AboutProjectModels;
using GarageMaster.Services.AboutProjects.Contracts;

namespace GarageMaster.Services.AboutProjects
{
    public class AboutProjectsService : IAboutProjectsService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AboutProjectsService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public AboutProjectModel GetAboutProject()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var aboutProject = unitOfWork.AboutProjects.GetAll().First();
                var aboutProjectModel = Mapper.Map<AboutProjectModel>(aboutProject);
                return aboutProjectModel;
            }
        }

        public void CreateAboutProject(AboutProjectCreateModel model)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var aboutProject = Mapper.Map<AboutProject>(model);
                
                unitOfWork.AboutProjects.Create(aboutProject);
            }
        }

        public AboutProjectEditModel GetAboutProjectForEdit()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var aboutProject = unitOfWork.AboutProjects.GetAll().First();
                return Mapper.Map<AboutProjectEditModel>(aboutProject);
            }
        }

        public void EditAboutProject(AboutProjectEditModel model)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var aboutProject = Mapper.Map<AboutProject>(model);
                
                unitOfWork.AboutProjects.Update(aboutProject);
            }
        }
    }
}
