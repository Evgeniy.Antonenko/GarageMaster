﻿using GarageMaster.Models.AboutProjectModels;

namespace GarageMaster.Services.AboutProjects.Contracts
{
    public interface IAboutProjectsService
    {
        void EditAboutProject(AboutProjectEditModel model);
        AboutProjectEditModel GetAboutProjectForEdit();
        void CreateAboutProject(AboutProjectCreateModel model);
        AboutProjectModel GetAboutProject();
    }
}
