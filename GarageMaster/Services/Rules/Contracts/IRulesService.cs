﻿using GarageMaster.Models.RulesModels;

namespace GarageMaster.Services.Rules.Contracts
{
    public interface IRulesService
    {
        void EditRule(RuleEditModel model);
        RuleEditModel GetRuleForEdit();
        void CreateRule(RuleCreateModel model);
        RuleModel GetRule();
    }
}
