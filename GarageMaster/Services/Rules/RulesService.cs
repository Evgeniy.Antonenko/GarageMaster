﻿using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.RulesModels;
using GarageMaster.Services.Rules.Contracts;

namespace GarageMaster.Services.Rules
{
    public class RulesService : IRulesService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RulesService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public RuleModel GetRule()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var rule = unitOfWork.Rules.GetAll().First();
                var ruleModel = Mapper.Map<RuleModel>(rule);
                return ruleModel;
            }
        }

        public void CreateRule(RuleCreateModel model)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var rule = Mapper.Map<Rule>(model);

                unitOfWork.Rules.Create(rule);
            }
        }

        public RuleEditModel GetRuleForEdit()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var rule = unitOfWork.Rules.GetAll().First();
                return Mapper.Map<RuleEditModel>(rule);
            }
        }

        public void EditRule(RuleEditModel model)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var rule = Mapper.Map<Rule>(model);

                unitOfWork.Rules.Update(rule);
            }
        }
    }
}
