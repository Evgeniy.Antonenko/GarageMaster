﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.ContactsModels;
using GarageMaster.Services.Contacts.Contracts;
using Microsoft.AspNetCore.Http;

namespace GarageMaster.Services.Contacts
{
    public class ContactsService : IContactsService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ContactsService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public ContactModel GetContact()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var contact = unitOfWork.Contacts.GetAll().First();
                var contactModel = Mapper.Map<ContactModel>(contact);
                return contactModel;
            }
        }

        public void CreateContact(ContactCreateModel model)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            model.Address = Regex.Replace(model.Address, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var contact = Mapper.Map<Contact>(model);
                
                contact.FilePath = UploadImage(model.UploadFile);

                unitOfWork.Contacts.Create(contact);
            }
        }

        public ContactEditModel GetContactForEdit()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var contact = unitOfWork.Contacts.GetAll().First();
                var contactModel = Mapper.Map<ContactEditModel>(contact);
                return contactModel;
            }
        }

        public void EditContact(ContactEditModel model)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            model.Address = Regex.Replace(model.Address, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var contactEditing = unitOfWork.Contacts.GetById(model.Id);
                var contact = GetContactForEdit(model, contactEditing);
                
                unitOfWork.Contacts.Update(contact);
            }
        }

        public Contact GetContactForEdit(ContactEditModel model, Contact contactEditing)
        {
            var contact = Mapper.Map(model, contactEditing);
            if (model.UploadFile != null)
            {
                if (contact.FilePath != null)
                {
                    string path = Directory.GetCurrentDirectory() + "/wwwroot" + contact.FilePath;
                    File.Delete(path);
                }
                contact.FilePath = UploadImage(model.UploadFile);
            }
            if (model.UploadFile == null)
            {
                if (contact.FilePath != null)
                {
                    string path = Directory.GetCurrentDirectory() + "/wwwroot" + contact.FilePath;
                    File.Delete(path);
                    contact.FilePath = contactEditing.FilePath;
                }
            }
            return contact;
        }

        private string UploadImage(IFormFile image)
        {
            string fileName = $"{Guid.NewGuid()}_{image.FileName}";
            string path = @"wwwroot";
            string subpath = @"uploads\ContactSection";
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string filePath = dirInfo.CreateSubdirectory(subpath).ToString();
            filePath = $@"wwwroot/uploads/ContactSection/{fileName}";
            using (var stream = System.IO.File.Create(filePath))
            {
                image.CopyTo(stream);
            }
            return $@"/uploads/ContactSection/{fileName}";
        }
    }
}
