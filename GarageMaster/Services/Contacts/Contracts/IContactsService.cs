﻿using GarageMaster.Models.ContactsModels;

namespace GarageMaster.Services.Contacts.Contracts
{
    public interface IContactsService
    {
        void EditContact(ContactEditModel model);
        ContactEditModel GetContactForEdit();
        void CreateContact(ContactCreateModel model);
        ContactModel GetContact();
    }
}
